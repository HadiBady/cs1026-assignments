# Developed by Hadi Badawi
# the purpose of this software is to calculate the volume of the a shape the user would like and then prints out the calculations
# at the end of the program


# importing the math module to use pi
import math


# declaring counters to execute the if print statements in the end
countQuit = 0
countCube = 0
countPyramid = 0
countEllipsoid = 0

# escape is used to escape from the while loop to end program
escape = True

# declaring strings to be used in the print statements
cubeInput = "cube"
cubeInputCap = "Cube"
pyramidInput = "pyramid"
pyramidInputCap = "Pyramid"
ellipsoidInput = "ellipsoid"
ellipsoidInputCap = "Ellipsoid"
quitInput = "quit"

# declaring lists to use for volume calculations
volumeCube = []
volumePyramid = []
volumeEllipsoid = []


## computes the volume of a cube
# @param s the length of a side of the cube
# @return the volume of the cube
#
def cube(s):
    volume = s ** 3
    volumeCube.append(volume)
    return volume

## computes the volume of a pyramid
# @param h the height of the pyramid
# @param b the base of the pyramid
# @return the volume of teh cube
#
def pyramid(b, h):
    volume = (1/3) * h * (b ** 2)
    volumePyramid.append(volume)
    return volume

## computes the volume of a ellipsoid
# @param r1 the first radius of the ellipsoid
# @param r2 the second radius of the ellipsoid
# @param r3 the third radius of the ellipsoid
# @return the volume of the ellipsoid
#math.pi is the number pi
#
def ellipsoid(r1, r2, r3):
    volume = (4/3) * math.pi * r1 * r2 * r3
    volumeEllipsoid.append(volume)
    return volume

## prints the final output for all volumes
# @param shapeInput the string for the shape's name
# @param shapeCount the number of volumes in the shape
# @param shapeList the list for the volume of the shape
#
def printOutput(shapeInput, shapeCount, shapeList):
    print(shapeInput + ": ", end="")
    shapeList.sort()
    for i in range(0, shapeCount):
            if i != (shapeCount - 1):
                print("%.2f" %(shapeList[i]), end=", ")
            else :
                print("%.2f" %(shapeList[i]))


#Start of the program
print("Calculate the Volume Software!")
choice = str(input("Please type in whether you want to calculate the volume of a cube, a pyramid, an ellipsoid, or quit:"))

#while loop for dummyproofing inputs
while choice != cubeInput and choice != pyramidInput and choice != ellipsoidInput and choice != quitInput :
    print("Invalid input!")
    choice = str(input("Please type in whether you want to calculate the volume of a cube, a pyramid, an ellipsoid, or quit:"))

# while loop for entering in the dimensions of each shape
while escape:
    if countQuit != 0:
        choice = str(input("Please type in whether you want to calculate the volume of a cube, a pyramid, an ellipsoid, or quit:"))

    if choice == cubeInput:
        countQuit += 1
        countCube += 1
        print("You have selected to calculate the volume of a", cubeInput + "!")
        widthCube = float(input("Please enter in the width of the " + cubeInput))
        print("The volume of a", cubeInput, "with a width of", widthCube, "is %.2f" %(cube(widthCube)))

    elif choice == pyramidInput:
        countQuit += 1
        countPyramid += 1
        print("You have selected to calculate the volume of a", pyramidInput)
        basePyramid = float(input("Please enter in the base of the " + pyramidInput))
        heightPyramid = float(input("Please enter in the height of the " + pyramidInput))
        print("The volume of a", pyramidInput, "with a base of", basePyramid, "and a height of", heightPyramid, "is %.2f" %(pyramid(basePyramid, heightPyramid)))

    elif choice == ellipsoidInput:
        countQuit += 1
        countEllipsoid += 1
        print("You have selected to calculate the volume of a", ellipsoidInput)
        r1 = float(input("Please enter in the first radius of the " + ellipsoidInput + ":"))
        r2 = float(input("Please enter in the second radius of the " + ellipsoidInput + ":"))
        r3 = float(input("Please enter in the third radius of the " + ellipsoidInput + ":"))
        print("The volume of a", ellipsoidInput, "with a r1 of", r1, ", r2 of", r2, ", and a r3 of", r3, "is %.2f" %(ellipsoid(r1, r2, r3)))

    elif choice == quitInput:
        escape = False


# the end of the program
print("You have come to the end of the session.")

# if statement to determine if the volumes of the shapes is printed out or not
if countQuit == 0:
    print("You did not perform any volume calculations")
else:
    print("The volumes calculated for each shape are shown below")
    if countCube > 0:
        printOutput(cubeInputCap, countCube, volumeCube)
    if countPyramid > 0:
        printOutput(pyramidInputCap, countPyramid, volumePyramid)
    if countEllipsoid > 0:
        printOutput(ellipsoidInputCap, countEllipsoid, volumeEllipsoid)
