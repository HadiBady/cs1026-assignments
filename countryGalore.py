class Country:
    def __init__(self, name, pop, area, continent):
        self._name = name
        self._pop = int(pop)
        self._area = float(area)
        self._continent = str(continent)

    def __repr__(self):
        strRep = self._name + " in " + self._continent
        return strRep

    def setPopulation(self, pop):
        self._pop = int(pop)

    def getName(self):
        return self._name

    def getArea(self):
        return self._area

    def getPopulation(self):
        return self._pop

    def getContinent(self):
        return self._continent

    def getPopDensity(self):
        popDensity = self._pop / self._area
        return popDensity


class CountryCatalogue:
    def __init__(self, continentFileName, countryFileName):
        self._cDictionary = {}
        self._catalogue = {}

        try:
            contFile = open(continentFileName, "r")
            dataFile = open(countryFileName, "r")

            contFile.readline()
            dataFile.readline()

            c = 0

            line = contFile.readline()
            line2 = dataFile.readline()

            while line != "":
                tempList = []
                tempList2 = []

                tempListPartial = []

                line = line.rstrip()
                line2 = line2.rstrip()

                tempList = line.split(",")
                tempList2 = line2.split("|")

                #tempList2[1] = tempList[1].replace(",", "")
                #tempList2[2] = tempList[1].replace(",", "")

                country = tempList2[0]
                population = tempList2[1].replace(",", "")
                area = tempList2[2].replace(",", "")
                continent = str(tempList[1])

                tempObject = Country(country, population, area , continent)

                self._catalogue[tempList2[0]]= (tempObject)
                self._cDictionary[tempList[0]] = tempList[1]

                line = contFile.readline()
                line2 = dataFile.readline()

        except FileNotFoundError:
            print("Error: File(s) are found.")

        finally:
            contFile.close()
            dataFile.close()


    def filterCountriesByContinent(self, continentName):
        tempList = []
        for name in self._cDictionary:
            if self._cDictionary[name] == continentName:
                tempList.append(name)
        return tempList

    def printCountryCatalogue(self):
        for c in self._catalogue:
            print(self._catalogue[c])

    def findCountry(self, countryName):
        for c in self._catalogue:
            if countryName == self._catalogue[c].getName():
                return self._catalogue[c]
        return None

    def deleteCountry(self, countryName):
        if countryName in self._cDictionary:
            self._cDictionary.pop(countryName)
            self._catalogue.pop(countryName)
            print("The country was successfully removed.")
        else:
            print("The country was not removed successfully.")

    def addCountry(self, countryName, countryPopulation, countryArea, countryContinent):
        if countryName in self._cDictionary:
            return False

        self._catalogue[countryName] = Country(countryName, countryPopulation, countryArea, countryContinent)

        self._cDictionary[countryName] = countryContinent

        return True

    def setPopulationOfASelectedCountry(self, countryName, countryPopulation):
        if countryName in self._cDictionary:
            match = ""
            if countryName in self._catalogue:
                    self._catalogue[countryName].setPopulation(countryPopulation)
            return True
        else:
            return False

    def saveCountryCatalogue(self, filename):
        file = open(filename, "w")

        file.write("Name|Continent|Population|PopulationDensity\n")
        i = 0
        for c in self._catalogue:
            if len(self._catalogue) > 0:
                toWrite = str(self._catalogue[c].getName() + "|" + self._catalogue[c].getContinent() + "|" + str(self._catalogue[c].getPopulation()) + "|" + "%.2f" %(self._catalogue[c].getPopDensity()) + "\n")
                file.write(toWrite)
                i += 1
        file.close()
        return i


    def findCountryWithLargestPop(self):
        largestPopName = ""
        largestPop = 0
        if len(self._catalogue) > 0:
            for i in self._catalogue:
                if float(self._catalogue[i].getPopulation()) > largestPop:
                    largestPopName = self._catalogue[i].getName()
                    largestPop = float(self._catalogue[i].getPopulation())

        return largestPopName

    def findCountryWithSmallestArea(self):
        i = 0
        for c in (self._catalogue):
            if i == 0:
                smallestAreaName = self._catalogue[c].getName()
                smallestArea = self._catalogue[c].getArea()
            if self._catalogue[c].getArea() < smallestArea:
                smallestAreaName = self._catalogue[c].getName()
                smallestArea = self._catalogue[c].getArea()
            i += 1

        return smallestAreaName

    def findMostPopulousContinent(self):
        tempDict = {}

        for countires in self._cDictionary:
            if self._cDictionary[countires] not in tempDict:
                tempDict[self._cDictionary[countires]] = 0

        i = 0

        for i in (self._cDictionary):
            tempDict[self._catalogue[i].getContinent()] += int(self._catalogue[i].getPopulation())

        tempList = []
        tempDict2 ={}
        tempPopDict = {}

        mostPop = 0
        mostPopCont = ""

        i = 0

        for c in tempDict:
            if i == 0:
                mostPop = tempDict[c]
                mostPopCont = c
            tempPop = tempDict[c]
            if tempPop > mostPop:
                mostPop = tempPop
                mostPopCont = c
            i+=1

        return(mostPopCont, mostPop)

    def filterCountriesByPopDensity(self, lowerBound, upperBound):
        tempList = []
        for c in (self._catalogue):
            popDensity = self._catalogue[c].getPopDensity()
            if popDensity >= lowerBound and popDensity <= upperBound:
                tempList.append(self._catalogue[c])
        return tempList
