# Developed by Hadi Badawi
# purpose of this program is to calculate the rental cost for cars

# Declaring Constants
B_RENTAL_PER_DAY = 20.0
D_RENTAL_PER_DAY = 50.0
B_AVG_KILO_DAILY = 100
W_RENTAL_PER_WEEK = 200.0
W_ABOVE_LIMIT_2_KILOS = 2000
W_ABOVE_LIMIT_1_KILOS = 1000
W_RENTAL_PER_WEEK_INCREASE_2 = 100.00
W_RENTAL_PER_WEEK_INCREASE_1 = 50.00
KILO_DRIVEN_CHARGE_RATE = 0.30
KILO_DRIVEN_BASE_CHARGE_RATE = 0.00
AGE_FOR_NO_EXTRA_CHARGE = 25
AGE_OVERCHARGE_RATE = 10.00


# Obtain information from the customer, including their name, age, classification code,
# the number of days vehicle was rented, and the start and end readings of the odometer
# for the rented vehicle.
name = str(input("Please enter the customer's name:"))
ageInt = int(input("Please enter the customer's age:"))
customerClassCode = str(input("Customer's Classification Code:"))

# if statement to ensure that the correct classification code is entered
if (customerClassCode != 'B' and \
                customerClassCode != 'b' and \
                customerClassCode != 'D' and \
                customerClassCode != 'd' and \
                customerClassCode != 'W' and \
                customerClassCode != 'w') :
    print('invalid Classification Code.')
    print("Customer name: " + name)
    print("Age: " + str(ageInt))
    exit()


#Acquiring the rest of the information for rental cost calculation
daysRented = int(input("Please enter the number of days the vehicle was rented:"))
odometerStart = int(input("Enter the start odometer reading of the vehicle:"))
odometerEnd = int(input("Enter the final odometer reading of the vehicle:"))

kilosDriven = odometerEnd - odometerStart # Defining variable to calculate the kilometers driven
avgKilosDaily = kilosDriven / daysRented # Calculation for the average kilometers driven per day

# condition and calculation to determine the number of weeks rented
if daysRented%7 != 0 :
    weeksRented = (daysRented//7) + 1
else :
    weeksRented = daysRented//7

# calculating the average kilometers driven within a week
avgKilosWeekly = kilosDriven / weeksRented

# The conditions associated with each classification code
if customerClassCode == 'B' or customerClassCode == 'b' :
    rentalPerDay = B_RENTAL_PER_DAY
    kiloDrivenCharge = KILO_DRIVEN_CHARGE_RATE

elif customerClassCode == 'D' or customerClassCode == 'd' :
    rentalPerDay = D_RENTAL_PER_DAY
# Condition to determine if the customer will be charged for the average kilometers driven over accepted value
    if avgKilosDaily > B_AVG_KILO_DAILY :
        kiloDrivenCharge = KILO_DRIVEN_CHARGE_RATE
    else :
        kiloDrivenCharge = KILO_DRIVEN_BASE_CHARGE_RATE

elif customerClassCode == 'W' or customerClassCode == 'w' :
    rentalPerWeek = W_RENTAL_PER_WEEK
     # to determine if the customer will be charged for the average kilometers driven over the accepted value and
     # if the rent per week is also increased due to different average kilometers driven
    if avgKilosWeekly > W_ABOVE_LIMIT_2_KILOS :
        rentalPerWeek = rentalPerWeek + W_RENTAL_PER_WEEK_INCREASE_2 #increase in the rental cost per week of 100 dollars
        kiloDrivenCharge = KILO_DRIVEN_CHARGE_RATE
    elif avgKilosWeekly > W_ABOVE_LIMIT_1_KILOS :
            rentalPerWeek = rentalPerWeek + W_RENTAL_PER_WEEK_INCREASE_1

# condition to perform the final calculation for the cost
if customerClassCode == 'B' or customerClassCode == 'b' :
    totalCost = (daysRented * rentalPerDay) + (kilosDriven * kiloDrivenCharge)

elif customerClassCode == 'D' or customerClassCode == 'd' :
    totalCost = (daysRented * rentalPerDay) + ((avgKilosDaily - B_AVG_KILO_DAILY) * kiloDrivenCharge * daysRented)

elif customerClassCode == 'W' or customerClassCode == 'w' :

    if avgKilosWeekly > W_ABOVE_LIMIT_2_KILOS :
       #only charged for how much kilometers driven above the normal average
       totalCost = (weeksRented * rentalPerWeek) + ((avgKilosWeekly - W_ABOVE_LIMIT_2_KILOS) * kiloDrivenCharge * weeksRented)
    else :
        totalCost = (weeksRented * rentalPerWeek)

# condition for customers under age 25
if ageInt < AGE_FOR_NO_EXTRA_CHARGE :
     totalCost = totalCost + (daysRented * AGE_OVERCHARGE_RATE)

#Print out Summary
print("Rental Bill:")
print("Customer's Name: " + name)
print("Customer's Age: " + str(ageInt))
print("Customer's Classification Code: " + str(customerClassCode))
print("Number of days vehicle was rented: " + str(daysRented))
print("Vehicle's odometer reading at the start of the rental period in kilometers: " + str(odometerStart))
print("Vehicle's odometer reading at the end of the rental period in kilometers: " + str(odometerEnd))
print("Total number of kilometers driven in the vehicle: " + str(kilosDriven))
print("Total Cost: $%.2f" %(totalCost))
